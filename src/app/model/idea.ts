export interface Idea {
  id: number;
  title: string;
  description: string;
  thumbnailUrl: string;
  goal?: string,
  publisher?: string,
  published?: string,
}
