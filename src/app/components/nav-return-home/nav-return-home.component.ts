import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav-return-home',
  templateUrl: './nav-return-home.component.html',
  styleUrls: ['./nav-return-home.component.scss']
})
export class NavReturnHomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
