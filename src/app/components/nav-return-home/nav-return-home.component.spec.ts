import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavReturnHomeComponent } from './nav-return-home.component';

describe('NavReturnHomeComponent', () => {
  let component: NavReturnHomeComponent;
  let fixture: ComponentFixture<NavReturnHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavReturnHomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavReturnHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
