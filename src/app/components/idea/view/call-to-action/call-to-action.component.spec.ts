import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewIdeaCallToActionComponent } from './call-to-action.component';

describe('ViewIdeaCallToActionComponent', () => {
  let component: ViewIdeaCallToActionComponent;
  let fixture: ComponentFixture<ViewIdeaCallToActionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewIdeaCallToActionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewIdeaCallToActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
