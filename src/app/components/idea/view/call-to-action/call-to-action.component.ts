import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'idea-view-call-to-action',
  templateUrl: './call-to-action.component.html',
  styleUrls: ['./call-to-action.component.scss']
})
export class ViewIdeaCallToActionComponent implements OnInit {

  public title:string = 'Do you have an idea that could better our world?';
  public text:string = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';
  public button_text:string = 'Share your idea';

  constructor() { }

  ngOnInit(): void {
  }

}
