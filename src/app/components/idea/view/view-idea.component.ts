import {Component, OnInit} from '@angular/core';
import {Idea} from "../../../model/idea";
import {IdeaService} from "../../../services/idea.service";
import {ActivatedRoute} from "@angular/router";
import {GOALS} from "../../../model/goal";

@Component({
  selector: 'app-view-idea',
  templateUrl: './view-idea.component.html',
  styleUrls: ['./view-idea.component.scss']
})
export class ViewIdeaComponent implements OnInit {

  idea: Idea = {id: -1, title: '', description: '', thumbnailUrl: ''};
  goals = GOALS;

  constructor(private ideaService: IdeaService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.ideaService.get(params.ideaId)
        .subscribe(idea => this.idea = idea);
    });
  }

  goal(key: string): string {
    const ideaGoal = this.goals.find(goal => goal.key === key);
    if(ideaGoal) return ideaGoal.label;
    return '';
  }

  published(): string {
    // eg; December 12th 2020
    const months = {
      0: 'January',
      1: 'February',
      2: 'March',
      3: 'April',
      4: 'May',
      5: 'June',
      6: 'July',
      7: 'August',
      8: 'September',
      9: 'October',
      10: 'November',
      11: 'December'
    }

    const notTh = {
      1: 'st',
      2: 'nd',
      3: 'rd',
      21: 'st',
      22: 'nd',
      23: 'rd',
      31: 'st'
    }

    const suffix = day => notTh[day] || 'th';

    if(this.idea?.published) {
      const date = new Date(this.idea.published);
      const month = months[date.getMonth()];
      const day = date.getDate();
      const year = date.getFullYear();
      const formatted = `${month} ${day}${suffix(day)} ${year}`;
      return formatted;
    } else {
      return undefined;
    }
  }

}
