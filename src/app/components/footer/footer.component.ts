import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  public footer_text:string = "We acknowledge the Traditional Owners of country throughout Australia and pay our respects to their Elders past, present and emerging.";
  public copyright_text:string = "Copyright © 2021 Digital Purpose";

  public social_media: string[] = ['LinkedIn', 'Facebook', 'Twitter'];
  constructor() { }

  ngOnInit(): void {
  }

}
