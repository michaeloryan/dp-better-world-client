import { Component, OnInit } from '@angular/core';
import { IdeaService } from '../../services/idea.service';
import { Idea } from '../../model/idea';
import { debounceTime, finalize } from 'rxjs/operators';
import { faSearch, faSpinner } from '@fortawesome/free-solid-svg-icons';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GOALS } from '../../model/goal';
import { ValidatorService } from './../../services/validator.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-submit-idea',
  templateUrl: './submit-idea.component.html',
  styleUrls: ['./submit-idea.component.scss'],
  // providers: [ValidatorService],
})
export class SubmitIdeaComponent implements OnInit {
  submitIdeaForm: FormGroup;
  goals = GOALS;
  disableSubmit = false;
  thumbnailUrl: string = '';
  loadingError: null;
  loading: boolean = false;

  constructor(
    private ideaService: IdeaService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.submitIdeaForm = this.formBuilder.group({
      title: ['', Validators.required],
      publisher: ['', Validators.required],
      goal: ['', Validators.required],
      description: ['', [Validators.required, Validators.maxLength(1000)]],
      thumbnailUrl: ['', [Validators.required, ValidatorService.url]],
    });

    this.submitIdeaForm.valueChanges
      .pipe(debounceTime(500))
      .subscribe(() => this.updateImage());
  }

  updateImage() {
    if (ValidatorService.url(this.submitIdeaForm.controls.thumbnailUrl) === null)
      this.thumbnailUrl = this.submitIdeaForm.value.thumbnailUrl;
  }

  thumbnail(): string {
    return this.thumbnailUrl;
  }

  submit(): void {
    this.disableSubmit = true;
    this.loading = true;
    if (this.submitIdeaForm.valid)
      this.ideaService
        .submit(this.submitIdeaForm.value)
        .pipe(finalize(() => this.loading = false))
        .subscribe(
          (response) => {this.router.navigateByUrl('/success')},
          (error) => {
            this.loadingError = error; 
            this.disableSubmit = false;
          }
        );
    else 
      this.disableSubmit = false;
  }
}
