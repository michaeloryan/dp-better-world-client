import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './components/app.component';
import {HomeComponent} from './components/home/home.component';
import {HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {ReactiveFormsModule} from "@angular/forms";
import {HeaderComponent} from './components/header/header.component';
import {ViewIdeaComponent} from './components/idea/view/view-idea.component';
import { ViewIdeaCallToActionComponent } from './components/idea/view/call-to-action/call-to-action.component';
import { NavReturnHomeComponent } from './components/nav-return-home/nav-return-home.component';
import { FooterComponent } from './components/footer/footer.component';
import { CallToActionComponent } from './components/call-to-action/call-to-action.component';
import { SubmitIdeaComponent } from './components/submit-idea/submit-idea.component';
import { ValidatorService } from './services/validator.service';
import { SuccessComponent } from './components/success/success.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    ViewIdeaComponent,
    ViewIdeaCallToActionComponent,
    NavReturnHomeComponent,
    FooterComponent,
    CallToActionComponent,
    SubmitIdeaComponent,
    SuccessComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    ReactiveFormsModule
  ],
  providers: [ValidatorService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
