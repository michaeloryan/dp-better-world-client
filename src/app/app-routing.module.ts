import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./components/home/home.component";
import {ViewIdeaComponent} from "./components/idea/view/view-idea.component";
import {SubmitIdeaComponent} from "./components/submit-idea/submit-idea.component";
import {SuccessComponent} from "./components/success/success.component";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: HomeComponent
  },
  {
    path: 'idea/:ideaId',
    component: ViewIdeaComponent
  },
  {
    path: 'submit',
    component: SubmitIdeaComponent
  },
  {
    path: 'success',
    component: SuccessComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
