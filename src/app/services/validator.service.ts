import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValidatorService {

  static getValidatorErrorMessage(validatorName: string, validatorValue?: any) {
    let config = {
      required: 'Required',
      validUrl: 'Url must begin with http:// or https://',
      maxlength: `Maximum length ${validatorValue.requiredLength}`,
    };

    return config[validatorName];
  }

  constructor() { }


  static url(control) {
    const http = 'http://';
    const https = 'https://';
    if (control.value.startsWith(http) || control.value.startsWith(https))
    {
      return null;
    } else {
      return { validUrl: true };
    }
  }
}
